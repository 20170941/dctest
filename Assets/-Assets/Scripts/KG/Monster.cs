﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Monster : MonoBehaviour
{
    public float mHP;
    public float mFullHP;

    public float HP { set { mHP = value; mHP_Image.fillAmount = mHP / mFullHP; } get { return mHP; } }
    public float FULLHP { set { mFullHP = value; } get { return mFullHP; } }

    public Image mHP_Image;


    public void setDamage(float _n)
    {
        HP -= _n;
    }
    
}
