﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class buttonpopup : MonoBehaviour {
 
    public GameObject targetObject;

    // 게임 오브젝트 활성화 여부 설정 함수     
    public void OnEnableSettingEvent()
    {

        // 오브젝트가 활성화된 상태이면 -> 비활성화  
        if (targetObject.activeInHierarchy)
            targetObject.SetActive(false);

        // 오브젝트가 비활성화된 상태이면 -> 활성화  
        else
            targetObject.SetActive(true);

    }
}