﻿using UnityEngine;
using System.Collections;
public class Control : MonoBehaviour
{
    // Update is called once per frame
    int speed = 10; //스피드 
    void Update()
    {
        float xMove = Input.GetAxis("Horizontal") * speed * Time.deltaTime; //x축으로 이동할 양
        this.transform.Translate(new Vector3(xMove, 0));  //이동

    }
}