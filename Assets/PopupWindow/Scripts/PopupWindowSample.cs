﻿using System;
using UnityEngine;

/// <summary>
/// 팝업 윈도우 샘플 스크립트
/// 샘플 스크립트를 참조하세요
/// </summary>
public class PopupWindowSample : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickOkButton()
    {
        string title = "Ok Popup Window";
        string message = "This Popup Window Show Ok";
        Action okAction = () => Debug.Log("On Click Ok Button");

        PopupWindowController.Instance.ShowOk(title, message, okAction);
    }

    public void OnClickOkCancelButton()
    {
        string title = "Ok/Cancel Popup Window";
        string message = "This Popup Window Show Ok/Cancel";
        Action okAction = () => Debug.Log("On Click Ok Button");
        Action cancelAction = () => Debug.Log("On Click Cancel Button");

        PopupWindowController.Instance.ShowOkCancel(title, message, okAction, cancelAction);
    }

    public void OnClickYesNoButton()
    {
        string title = "Yes/No Popup Window";
        string message = "This Popup Window Show Yes/No";
        Action yesAction = () => Debug.Log("On Click Yes Button");
        Action noAction = () => Debug.Log("On Click No Button");

        PopupWindowController.Instance.ShowYesNo(title, message, yesAction, noAction);
    }

    public void OnClickYesNoCancelButton()
    {
        string title = "Yes/No/Cancel Popup Window";
        string message = "This Popup Window Show Yes/No/Cancel";
        Action yesAction = () => Debug.Log("On Click Yes Button");
        Action noAction = () => Debug.Log("On Click No Button");
        Action cancelAction = () => Debug.Log("On Click Cancel Button");

        PopupWindowController.Instance.ShowYesNoCancel(title, message, yesAction, noAction, cancelAction);
    }
}
