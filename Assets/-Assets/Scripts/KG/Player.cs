﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Image")]
    public SpriteRenderer mSpriteRenderer;
    public List<Sprite> mMoveSprites;
    public List<Sprite> mAttackSprites;

    [Header("Point")]
    public Transform mStartPos;
    public Transform mEndPos;

    [Header("Data")]
    public float mPower = 0.25f;

    public Monster mMonster;



    public enum AnimationType { 
        None,
        Move,
        Attack
    };

    public AnimationType mAnimationType = AnimationType.None;

    public void Start()
    {
        StartCoroutine(@LoopAnimationCoroutine());
    }

    IEnumerator @LoopAnimationCoroutine()
    { 
        while(true)
        {
            //몬스터 레벨링
            //이미지 변경
            mMonster.HP = 100;
            mMonster.FULLHP = 100;


            setAnimationType(AnimationType.Move);
            yield return StartCoroutine(@MoveCoroutine(mStartPos.localPosition, mEndPos.localPosition, 1));


            while (mMonster.mHP > 0)
            {
                mMonster.setDamage(mPower);
                setAnimationType(AnimationType.Attack);
                yield return new WaitForSeconds(0.5f);
            }
        }
    }


    public void setAnimationType(AnimationType _animationType)
    {
        if (mAnimationCoroutine != null)
        {
            StopCoroutine(mAnimationCoroutine);
            mAnimationCoroutine = null;
        }
        mAnimationCoroutine = StartCoroutine(@AnimationCoroutine(_animationType));

        mAnimationType = _animationType;
    }


    Coroutine mAnimationCoroutine = null;

    IEnumerator @AnimationCoroutine(AnimationType _animationType)
    {
        List<Sprite> list = null;

        switch (_animationType)
        {
            case AnimationType.Move:
                list = mMoveSprites;
                break;
            case AnimationType.Attack:
                list = mAttackSprites;
                break;
        }

        if (list != null)
        {
            int count = 0;
            while (true)
            {

                mSpriteRenderer.sprite = list[count];
                count++;

                if (list.Count <= count)
                    count = 0;

                yield return new WaitForSeconds(0.25f);

            }
        }
    }

    IEnumerator @MoveCoroutine(Vector3 _start, Vector3 _end, float _time)
    {
        float timer = 0;
        while(_time > timer)
        {
            mSpriteRenderer.transform.localPosition = Vector3.Lerp(_start, _end, timer / _time);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
        
    }

}
